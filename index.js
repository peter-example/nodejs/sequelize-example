const Sequelize = require('sequelize');

let sequelize = new Sequelize('test1', 'test1', 'test1', {
	host: 'localhost',
	dialect: 'mysql'
});

// sequelize.authenticate().then(() => {
// 	console.log('ok');
// }).catch((err) => {
// 	console.log(err);
// });

const User = sequelize.define('user', {
	username: {
		type: Sequelize.DataTypes.STRING,
		allowNull: false
	},
	password: {
		type: Sequelize.DataTypes.STRING,
	},
	age: {
		type: Sequelize.DataTypes.INTEGER,
		defaultValue: 21
	}
},
	{
		freezeTableName: true
	}
);

User.sync({ alter: true }).then(() => {
	for (x = 0; x < 10000; x++) {
		const user = User.create({ username: "mcheung63@hotmail.com", password: "123" });
		console.log(user);
		// user.save();
	}
});

